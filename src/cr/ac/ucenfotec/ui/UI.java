package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.CL;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class UI {

    static private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static private CL gestor = new CL();

    public static void main(String[] args) throws Exception{
        mostrarMenu();
    }

    public static void mostrarMenu() throws Exception{
        int opcion =0;
        do {
            System.out.println("******** Bienvenido al sistema de Matricula ****** ");
            System.out.println("1. Registrar Carrera.");
            System.out.println("2. Listar Carreras.");
            System.out.println("3. Registrar Curso.");
            System.out.println("4. Listar Cursos.");
            System.out.println("5. Agregar un Curso a una Carrera.");
            System.out.println("6. Registrar un Grupo a un Curso.");
            System.out.println("7. Registrar Profesor.");
            System.out.println("8. Listar Profesores.");
            System.out.println("9. Agregar Profesor a Grupo.*** ");
            System.out.println("10.Registrar Estudiante.");
            System.out.println("11.Listar Estudiantes.");
            System.out.println("12.Agregar Estudiante a Grupo. *** ");
            System.out.println("13.Registrar Funcionario. ");
            System.out.println("14.Listar Funcionarios. ");
            System.out.println("0. Salir");
            System.out.print("Por favor digite la opción: ");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        }while(opcion !=0);
    }

    public static void procesarOpcion(int opcion) throws Exception{
        switch (opcion){
            case 1: registrarCarrera();
                    break;
            case 2: listarCarreras();
                    break;
            case 3: registrarCurso();
                break;
            case 4: listarCursos();
                break;
            case 5: agregarCursoACarrera();
                break;
            case 6: registrarGrupo();
                break;
            case 0:
                System.out.println("Gracias por su visita");
            default:
                System.out.println("Opción invalida!");
        }
    }

    public static void registrarCarrera() throws Exception{
        System.out.print("Digite el codigo de la carrera: ");
        String codigo = in.readLine();
        System.out.print("Digite el nombre de la carrera: ");
        String nombre = in.readLine();
        System.out.print("Digite si la carrera es acreditada (1-Sí, 0-No): ");
        boolean esAcreditada = in.readLine().equals("1")?true:false;

        //llamar a alguien que se encargue de crear el objeto y almacenarlo en algún lugar
        String resultado = gestor.registrarCarrera(codigo,nombre,esAcreditada);
        System.out.println(resultado);
    }

    public static void listarCarreras() throws Exception{
        System.out.println("*****Listado de Carreras inscritas *****");
        String[] datos = gestor.listarCarreras();
        for (int i =0;i < datos.length ;i++) {
            System.out.println(datos[i]);
        }
    }

    public static void registrarCurso()throws Exception{
        System.out.print("Digite el codigo del curso: ");
        String codigo = in.readLine();
        System.out.print("Digite el nombre del curso: ");
        String nombre = in.readLine();
        System.out.print("Digite la cantidad de créditos: ");
        int creditos = Integer.parseInt(in.readLine());
        System.out.print("Digite el costo del curso: ");
        double costo = Double.parseDouble(in.readLine());

        gestor.registrarCurso(codigo,nombre,creditos,costo);
    }

    public static void listarCursos() throws Exception{
        System.out.println("*****Listado de Cursos inscritos *****");
        String[] datos = gestor.listarCursos();
        for (int i =0;i < datos.length ;i++) {
            System.out.println(datos[i]);
        }
    }

    public static void agregarCursoACarrera() throws Exception{
        System.out.print("Por favor digite el código de la carrera: ");
        String codigoCarrera = in.readLine();
        System.out.print("Por favor digite el código del curso: ");
        String codigoCurso = in.readLine();

        String resultado = gestor.agregarCursoACarrera(codigoCarrera,codigoCurso);
        System.out.println(resultado);
    }

    public static void registrarGrupo() throws Exception{
        System.out.print("Por favor digite el codigo del curso: ");
        String codigoCurso = in.readLine();
        System.out.print("Por favor digite el número de grupo: ");
        int numero=Integer.parseInt(in.readLine());
        System.out.print("Por favor digite el código de grupo: ");
        String codigoGrupo = in.readLine();

        String resultado = gestor.registrarGrupo(codigoCurso,numero,codigoGrupo);
        System.out.println(resultado);
    }
}












