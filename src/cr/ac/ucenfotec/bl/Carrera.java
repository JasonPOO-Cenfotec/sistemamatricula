package cr.ac.ucenfotec.bl;

import java.util.ArrayList;
import java.util.Objects;

public class Carrera {

    private String codigo;
    private String nombre;
    private boolean esAcreditada;
    private ArrayList<Curso> listaCursos;//relación de agregación multiple

    public Carrera(String codigo, String nombre, boolean esAcreditada) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.esAcreditada = esAcreditada;
        this.listaCursos = new ArrayList<>();
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEsAcreditada() {
        return esAcreditada;
    }

    public void setEsAcreditada(boolean esAcreditada) {
        this.esAcreditada = esAcreditada;
    }

    public void agregarCurso(Curso curso){
        listaCursos.add(curso);
    }


    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (getClass() != o.getClass()) return false;
        Carrera carrera = (Carrera) o;
        return codigo.equals(carrera.codigo);
    }

    public String toString() {
        String mensaje = "Carrera codigo: " + codigo + '\n';
        mensaje += "Nombre: " + nombre +", es Acreditada: " +esAcreditada+"\n";

        for (Curso curso:listaCursos) {
            mensaje += curso.toString() + "\n";
        }

        return mensaje;
    }
}
