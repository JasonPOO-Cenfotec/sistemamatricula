package cr.ac.ucenfotec.bl;

import java.util.ArrayList;

public class Curso {

    private String codigo;
    private String nombre;
    private int creditos;
    private double costo;
    private ArrayList<Grupo> listaGrupos; //relación de Composición multiple

    public Curso() {
    }

    public Curso(String codigo, String nombre, int creditos, double costo) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.creditos = creditos;
        this.costo = costo;
        this.listaGrupos = new ArrayList<>();
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public void agregarGrupo(int numero, String codigo){
        Grupo grupo = new Grupo(numero,codigo);
        listaGrupos.add(grupo);
    }

    public String toString() {
        String mensaje= "Curso codigo: " + codigo + "\n";
        mensaje += "Nombre: " + nombre +"\n";
        mensaje += "Créditos: " + creditos +"\n";
        mensaje += "Costo: " + costo+"\n";
        mensaje += "------- Grupos -------\n";

        for (int i=0; i < listaGrupos.size();i++) {
            mensaje += listaGrupos.get(i) + "\n";
        }
        return mensaje;
    }
}
