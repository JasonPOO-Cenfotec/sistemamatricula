package cr.ac.ucenfotec.bl;

import java.time.LocalDate;

public abstract class Persona {

    private int cedula;
    private String nombre;
    private int edad;
    private String genero;
    private LocalDate fechaNacimiento;

    public Persona() {
    }

    public Persona(int cedula, String nombre, int edad, String genero, LocalDate fechaNacimiento) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.edad = edad;
        this.genero = genero;
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public abstract String registarIngreso();

    public String toString() {
        return  "cedula=" + cedula +
                ", nombre=" + nombre +
                ", edad=" + edad +
                ", genero=" + genero +
                ", fechaNacimiento=" + fechaNacimiento;
    }
}
