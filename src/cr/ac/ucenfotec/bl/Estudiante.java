package cr.ac.ucenfotec.bl;

import java.time.LocalDate;

public class Estudiante extends Persona{

    public Estudiante() {
    }

    public Estudiante(int cedula, String nombre, int edad, String genero, LocalDate fechaNacimiento) {
        super(cedula, nombre, edad, genero, fechaNacimiento);
    }

    public String registarIngreso() {
        return "Id de ingreso del estudiante: " + (getCedula()*4545)/3;
    }

    public String toString() {
        return super.toString();
    }
}
