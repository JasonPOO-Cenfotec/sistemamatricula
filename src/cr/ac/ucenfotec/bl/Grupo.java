package cr.ac.ucenfotec.bl;

import java.util.ArrayList;

public class Grupo {

    private int numero;
    private String codigo;
    private Profesor profesor; //relación de agregación simple.
    private ArrayList<Estudiante> listaEstudiantes; //relación de agregación multiple

    public Grupo() {
    }

    public Grupo(int numero, String codigo) {
        this.numero = numero;
        this.codigo = codigo;
        this.listaEstudiantes = new ArrayList<>();
    }

    public Grupo(int numero, String codigo, Profesor profesor) {
        this.numero = numero;
        this.codigo = codigo;
        this.profesor = profesor;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

    public void agregarEstudiante(Estudiante estudiante){
        listaEstudiantes.add(estudiante);
    }

    public String toString() {
        String mensaje = "----- Grupo # " + numero +" ----\n";
        mensaje += "Codigo: " + codigo + "\n";
        mensaje += "Profesor: " +profesor + "\n";
        mensaje += "------- Lista de Estudiantes ----\n";

        for(Estudiante estu:listaEstudiantes){
            mensaje += estu.toString() + "\n";
        }
        return mensaje;
    }
}
























