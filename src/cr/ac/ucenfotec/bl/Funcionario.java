package cr.ac.ucenfotec.bl;

import java.time.LocalDate;

public class Funcionario extends Persona{

    private String tipoNombramiento;
    private String departamento;

    public Funcionario() {
    }

    public Funcionario(int cedula, String nombre, int edad, String genero, LocalDate fechaNacimiento, String tipoNombramiento, String departamento) {
        super(cedula, nombre, edad, genero, fechaNacimiento);
        this.tipoNombramiento = tipoNombramiento;
        this.departamento = departamento;
    }

    public String getTipoNombramiento() {
        return tipoNombramiento;
    }

    public void setTipoNombramiento(String tipoNombramiento) {
        this.tipoNombramiento = tipoNombramiento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String registarIngreso() {
        return "Id de ingreso del Funcionario: " + (departamento.length() + getCedula() *80)/5;
    }

    public String toString() {
        return super.toString() +" ,tipoNombramiento='" + tipoNombramiento +", departamento='" + departamento;
    }
}
