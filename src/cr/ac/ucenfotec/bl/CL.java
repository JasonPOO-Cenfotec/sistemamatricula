package cr.ac.ucenfotec.bl;

import java.time.LocalDate;
import java.util.ArrayList;

public class CL {

    private ArrayList<Carrera> listaCarreras;
    private ArrayList<Curso> listaCursos;
    private ArrayList<Persona> listaPersonas;

    public CL(){
        listaCarreras = new ArrayList<>();
        listaCursos = new ArrayList<>();
    }

    public String registrarCarrera(String codigo, String nombre, boolean esAcreditada){
        Carrera nuevaCarrera = new Carrera(codigo,nombre,esAcreditada);
        if(!listaCarreras.contains(nuevaCarrera)){
            listaCarreras.add(nuevaCarrera);
            return "Carrera registrada correctamente!";
        }else{
            return "La carrera que esta ingresando ya existe en el sistema!";
        }
    }

    public String[] listarCarreras(){
        String[] lista = new String[listaCarreras.size()];
        for(int i =0; i< listaCarreras.size() ;i++){
            lista[i] = listaCarreras.get(i).toString();
        }
        return lista;
    }

    public void registrarCurso(String codigo, String nombre, int creditos, double costo){
        Curso curso = new Curso(codigo,nombre,creditos,costo);
        listaCursos.add(curso);
    }

    public String[] listarCursos(){
        String[] lista = new String[listaCursos.size()];
        for(int i =0; i< listaCursos.size() ;i++){
            lista[i] = listaCursos.get(i).toString();
        }
        return lista;
    }

    public String agregarCursoACarrera(String codigoCarrera, String codigoCurso){
        // 1. Buscar una Carrera por el codigo
        // 2. En caso de no encontrar la carrera devolver mensaje "Carrera no existe en el sistema!"
        // 3. Buscar el curso por el código
        // 4. En caso de no encontrar el curso  devolver un mensaje "Curso no existe en el sistema!"
        // 5. Agregar el curso a la carrera
        Carrera carrera = buscarCarrera(codigoCarrera);
        if(carrera != null){
            Curso curso = buscarCurso(codigoCurso);
            if(curso != null){
                carrera.agregarCurso(curso);
                return "El curso fue agregado de manera correcta!";
            }else
                return "Curso no existe en el sistema!";
        }else
            return "Carrera no existe en el sistema!";
    }

    private Carrera buscarCarrera(String codigoCarrera){
        for(Carrera carrera:listaCarreras){
            if(carrera.getCodigo().equals(codigoCarrera)){
                return carrera;
            }
        }
        return null;
    }

    private Curso buscarCurso(String codigoCurso){
        for(Curso curso:listaCursos){
            if(curso.getCodigo().equals(codigoCurso)){
                return curso;
            }
        }
        return null;
    }

    public String registrarGrupo(String codigoCurso,int numero,String codigoGrupo){
        // 1. Buscar el curso por el código
        // 2. En caso de no encontrar el curso  devolver un mensaje "Curso no existe en el sistema!"
        Curso curso = buscarCurso(codigoCurso);
        if(curso != null){
            curso.agregarGrupo(numero,codigoGrupo);
            return "El grupo fue agregado de manera correcta!";
        }else
            return "Curso no existe en el sistema!";
    }

    public void registarEstudiante(int cedula, String nombre, int edad, String genero, LocalDate fechaNacimiento){
        Estudiante estudiante = new Estudiante(cedula,nombre,edad,genero,fechaNacimiento);
        listaPersonas.add(estudiante);
    }

    public void registarProfesor(int cedula, String nombre, int edad, String genero, LocalDate fechaNacimiento, String profesion){
        Profesor profesor = new Profesor(cedula,nombre,edad,genero,fechaNacimiento,profesion);
        listaPersonas.add(profesor);
    }

    public void registarFuncionario(int cedula, String nombre, int edad, String genero, LocalDate fechaNacimiento, String tipoNombramiento, String departamento){
       Funcionario funcionario = new Funcionario(cedula,nombre,edad,genero,fechaNacimiento,tipoNombramiento,departamento);
       listaPersonas.add(funcionario);
    }

    public ArrayList<String> listarEstudiantes(){
        ArrayList<String> lista = new ArrayList<>();
        for (Persona persona:listaPersonas) {
            if(persona instanceof Estudiante){
                lista.add(persona.toString());
            }
        }
        return lista;
    }
}































