package cr.ac.ucenfotec.bl;

import java.time.LocalDate;

public class Profesor extends Persona {

    private String profesion;

    public Profesor() {
    }

    public Profesor(int cedula, String nombre, int edad, String genero, LocalDate fechaNacimiento, String profesion) {
        super(cedula, nombre, edad, genero, fechaNacimiento);
        this.profesion = profesion;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String registarIngreso() {
        return "Id de ingreso del Profesor: " + (profesion.length() *80)/5;
    }

    public String toString() {
        return super.toString() + ", profesion='" + profesion;
    }
}
